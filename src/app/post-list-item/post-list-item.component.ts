import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../models/post-model';
import { PostService } from '../services/post-service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent implements OnInit {

  @Input() post: Post;
  @Input() id: number;


  constructor(private postService: PostService) { }

  ngOnInit() {
  }

  onLoveIt() {
    this.postService.updatePostLoveIts(this.id, +1);
  }

  onDontLoveIt() {
    this.postService.updatePostLoveIts(this.id, -1);
  }

  onDelete() {
    console.log('delete !' + this.id);
    this.postService.deletePost(this.id);

  }

  getLoveIts() {
    return this.post.loveIts;
  }

  getColor() {
    if (this.post.loveIts > 0) {
      return 'green';
    } else if (this.post.loveIts < 0) {
      return 'red';
    }
  }

}

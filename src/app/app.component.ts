import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor() {
    const firebaseConfig = {
      apiKey: 'AIzaSyCr3gtWcu2aBnWJiLD1dVEgvkFBYKDGYTY',
      authDomain: 'oc-blog-65c0e.firebaseapp.com',
      databaseURL: 'https://oc-blog-65c0e.firebaseio.com',
      projectId: 'oc-blog-65c0e',
      storageBucket: 'oc-blog-65c0e.appspot.com',
      messagingSenderId: '751867638006',
      appId: '1:751867638006:web:f7c6d7634dbc8c8ead7cd7'
    };

    firebase.initializeApp(firebaseConfig);
  }
}

import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Post } from '../models/post-model';
import { PostService } from '../services/post-service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})

export class PostListComponent implements OnInit, OnDestroy {

  posts: Post[];
  postSubscribtion: Subscription;

  constructor(private postService: PostService) {
  }

  ngOnInit() {
    this.postSubscribtion = this.postService.postSubject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    );
    this.postService.emitPost();
  }

  ngOnDestroy() {
    this.postSubscribtion.unsubscribe();
  }

}

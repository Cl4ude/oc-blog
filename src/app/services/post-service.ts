import { Post } from '../models/post-model';
import { Subject } from 'rxjs';
import * as firebase from 'firebase';
import Datasnapshot = firebase.database.DataSnapshot;

export class PostService {

    private posts: Post[] = [];

    postSubject = new Subject<Post[]>();

    constructor() {
        this.getPost();
    }

    emitPost() {
        this.postSubject.next(this.posts.slice());
    }

    savePost() {
        firebase.database().ref('/posts').set(this.posts);
    }

    getPost() {
        firebase.database().ref('/posts')
          .on('value', (data: Datasnapshot) => {
              this.posts = data.val() ? data.val() : [];
              this.emitPost();
            }
          );
      }

    addPost(newPost: Post) {
        this.posts.push(newPost);
        this.savePost();
        this.emitPost();
    }

    deletePost(postId: number) {
        this.posts.splice(postId, 1);
        this.savePost();
        this.emitPost();
    }

    updatePostLoveIts(postId: number, valueAdded: number) {
        this.posts[postId].loveIts = this.posts[postId].loveIts + valueAdded;
        this.savePost();
        this.emitPost();
    }
}
